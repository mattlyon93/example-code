'''
Backup Tool v2
'''
import os
import win32api
import shutil

Directories = {
    'Documents': 'Documents',
    'Music': 'Music',
    'Pictures': 'Pictures',
    'Projects': 'Projects',
    }

dataDrives = {
    'Media': None,
    'Data': None
    }

backupDrive = {
    'Backup': None
    }

def CheckDirectories(DataDrives):
    '''Checks what directories exist/are plugged in'''
    #Finds names of drives
    systemDrives = win32api.GetLogicalDriveStrings().split('\000')[:-1]
    for ddrive in DataDrives:
        for sdrive in systemDrives:
            try:
                if win32api.GetVolumeInformation(sdrive)[0] == ddrive:
                    DataDrives[ddrive] = sdrive
            except:
                pass

    return DataDrives


class BackupDrive():
    '''
    Class to Backup Files  
    '''
    def __init__(self,dataDrive,backupDrive,directoryDict):
        '''Initalises Backup Class'''
        self.dirDict = directoryDict
        self.Drive = dataDrive
        self.bDrive = backupDrive
        self.dirsToBackup = []
        self.fileDict = {}
        self.toCopy = []
        
    def FindDirectorysToBackup(self):
        '''Finds top level directories to backup & adds to list'''
        for iDir in self.dirDict:
            dirPath = os.path.join(self.Drive,iDir)
            if os.path.isdir(dirPath):
                self.dirsToBackup.append(dirPath)
    
    def WalkThroughDirectory(self):
        '''Walks through directories, grabs filepath & time modified into dict'''
        for iDir in self.dirsToBackup:
            for root, dirs, files in os.walk(iDir):
                for name in files:
                    #Not backing up temp files
                    if not name.endswith('.tmp') and not name.startswith('~$'):
                        fp = os.path.join(root.split('\\',1)[1],name)
                        self.fileDict[fp] = os.path.getmtime(os.path.join(self.Drive,fp))

    def CompareBackup(self):
        '''Compares files in data drive & backup drive, collects ones that have been modified since last backup'''
        for fp in self.fileDict:
            bfp = os.path.join(self.bDrive,fp)
            if os.path.isfile(bfp):
                bMod = os.path.getmtime(os.path.join(self.bDrive,fp))
                if not bMod == self.fileDict[fp]:
                    self.toCopy.append(fp)
            else:
                print("%s doesn't exist in Backup" %fp)
                self.toCopy.append(fp)
        print(self.toCopy)
        
    def CopyToBackup(self):
        '''Copies files in list to backup, creates directory if necessary'''
        for file in self.toCopy:
            print("Copying %s" %file)
            try:
                shutil.copy2(os.path.join(self.Drive,file), os.path.join(self.bDrive,file))
            except IOError as e:
                if e.errno == 2:
                    try:
                        os.makedirs(os.path.join(self.bDrive,os.path.dirname(file)))
                        shutil.copy2(os.path.join(self.Drive,file), os.path.join(self.bDrive,file))
                    except Exception as e:
                        print(e)
                    else:
                        print("Done")
            else:
                print("Done")
                
    def run(self):
        self.FindDirectorysToBackup()
        self.WalkThroughDirectory()
        self.CompareBackup()
        self.CopyToBackup()       

dataDrives = CheckDirectories(dataDrives)
backupDrive = CheckDirectories(backupDrive)
for Drive in dataDrives:
    if dataDrives[Drive]:
        print("'%s' Drive plugged in at %s" %(Drive,dataDrives[Drive]))
        if backupDrive['Backup']:
            print('Backup Drive plugged in at %s' %backupDrive['Backup'])
        else:
            print('Backup Drive not plugged in, exiting')
            exit()
        Backup = BackupDrive(dataDrives[Drive],backupDrive['Backup'],Directories)
        Backup.run()

        
     

