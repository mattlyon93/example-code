#!/usr/bin/env python
'''
Created on 20/02/2019

Class to process DKE (Currently only on the Cluster)

@author: Matt Lyon
'''

import stilCluster.rocksDB as rDB
import os


class DKE(object):
    ''' Generalized DKE methods
    @param stil_id: string, STIL ID
    @param diffusion_file: string, path on RDS to the diffusion file
    @param bval_list: list of strings, each item of the list is a bvalue found in the diffusion data, do not include b0
    '''
    ## CONSTANTS
    TEMPLATE_FILEPATH = '/RDSMount/DKE/kurtosis_cluster_template.sh'
    DKE_PARAMS_FILEPATH = '/RDSMount/DKE/dke_parameters_nointerp_hcp.dat'
    
    def __init__(self, stil_id, diffusion_file, bval_list):

        self.stil_id = stil_id
        self.bval_list = bval_list
        self.diffusion_file = diffusion_file
        
class DKE_Local(DKE):
    '''Class to handle running of DKE locally, not currently built
    TODO
    '''
    def __init__(self, stil_id, diffusion_file, bval_list):
        
        super(DKE_Cluster,self).__init__(stil_id, diffusion_file, bval_list)
    
class DKE_Cluster(DKE):
    '''Class to handle running of DKE pipeline specifically on the Cluster
    @param output_dir: directory on RDS to output DKE files
    '''
    
    ## CONSTANTS
    MCR_CLUSTER_PATH = '/state/partition1/MCR/v717'
    DKE_CLUSTER_PATH = '/share/apps/DKI/bin'
    DKE_CLUSTER_DATA_DIR = '/mnt/data/DKI'
    DKE_CLUSTER_JOBDIR = '/home/stil_user/TESTING/jobs/DKE'
    DKE_RDS_LOGDIR = os.path.join(rDB.RCOS_ADDRESS, 'PRJ-STILNeuro/PROCESSING/DKE/Archive')
    
    def __init__(self, stil_id, diffusion_file, bval_list, rds_output_dir):
        self.rds_output_dir = self._getRcosOutputDir(rds_output_dir)
        super(DKE_Cluster,self).__init__(stil_id, diffusion_file, bval_list)
        self._checkRuntimeFiles()
        
    def _getRcosOutputDir(self, output_dir):
        '''Trims the suffix of the RDS path to allow for rcos transfer method
        @return output_dir: string of rcos output path
        '''
        if output_dir.startswith('/RDSMount/'):
            return os.path.join(rDB.RCOS_ADDRESS, output_dir[10:])
        else:
            raise OSError('RDS not mounted at /RDSMount, unable to upload DKE data')
        
    def _checkRuntimeFiles(self):
        '''Checks the template and DKE parameters files are available'''
        assert os.path.isfile(self.TEMPLATE_FILEPATH), 'Cannot find template file: %s' %(self.TEMPLATE_FILEPATH)
        assert os.path.isfile(self.DKE_PARAMS_FILEPATH), 'Cannot find DKE parameters file: %s' %(self.DKE_PARAMS_FILEPATH)
        assert self.DKE_PARAMS_FILEPATH.startswith('/RDSMount/'), 'DKE Parameters file not located on /RDSMount'
        
    def _modifyTemplate(self):
        '''Builds the qsub runscript to run DKE on the cluster
        @return string: filepath to modified template file
        '''

        # Here we must check that the Diffusion file lives on an /RDSMount, if not the scp will not work properly
        if self.diffusion_file.startswith('/RDSMount/'):
            suffix_dir = self.diffusion_file[10:]
        else:
            raise OSError('Diffusion file does not live on /RDSMount, unable to copy file correctly')
        
        # Setup inputs
        inputs_dict = {'output_dir': '"%s"' %(os.path.join(self.DKE_CLUSTER_DATA_DIR, self.stil_id)),
                       'rds_diffusion_path': '"%s"' %(os.path.join(rDB.RCOS_ADDRESS, suffix_dir)),
                       'dke_install_dir': '"%s"' %(self.DKE_CLUSTER_PATH),
                       'matlab_install_dir': '"%s"' %(self.MCR_CLUSTER_PATH),
                       'bvals': '"%s"' %(' '.join(self.bval_list)),
                       'dke_params_tmp': '"%s"' %(os.path.join(rDB.RCOS_ADDRESS, self.DKE_PARAMS_FILEPATH[10:])),
                       'rds_output_dir': '"%s"' %(self.rds_output_dir),
                       'rds_log_dir': '"%s"' %(self.DKE_RDS_LOGDIR)
                       }
        
        # Grab template input as string
        fp = open(self.TEMPLATE_FILEPATH, 'r')
        template_str_list = fp.readlines()
        fp.close()

        # Insert inputs
        for index, key in enumerate(inputs_dict):
            template_str_list.insert(index+4, '%s=%s\n' %(key, inputs_dict[key]))
        template_str = ''.join(template_str_list)    
        return template_str
    
    def runOnCluster(self):
        '''Highest level function, calls scripts in order'''
        ClusterObj = rDB.CLUSTER_DB(job_name='%s_DKE' %(self.stil_id), 
                                    working_dir=os.path.join(self.DKE_CLUSTER_JOBDIR, self.stil_id))
        template_str = self._modifyTemplate()
        ClusterObj.buildRunScript(run_string=template_str, queue_name='dke.q')
        ClusterObj.createDirOnCluster(os.path.join(self.DKE_CLUSTER_JOBDIR, self.stil_id))
        ClusterObj.copyRunScriptToCluster()
        ClusterObj.submitToQueue()
        