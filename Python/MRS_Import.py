#!/usr/bin/env python
'''
Tool to import MRS data to the RDS

@author Matt Lyon
'''
import os
import shutil
import argparse
import stilpy.stilRedcap.RedCapTools as rdt
from stilpy.stilPfile import headers, struct_utils, anonymizer

### METHODS ###
def findPFiles(DIR):
    if not os.path.isdir(DIR):
        raise OSError('%s is not a valid directory' %DIR)
    fileList = []
    for root, _, files in os.walk(DIR):
        for name in files:
            if name.endswith('.7'):
                fileList.append(os.path.abspath(os.path.join(root,name)))
    if fileList:
        print 'Found P-files:'
        for pFile in fileList:
            print(pFile)
        print '\n\n'
    else:
        print 'No P-files found, exiting'
        exit()
    return fileList

def createMRSDict():
    MRSDict = {'psd_name': None,
            'exam_number': None,
            'date_of_birth': None,
            'te':None,
            'series_description':None
            }
    return MRSDict

def matchIDs(data,enum,dob,id_label):
    for _, row in data.iterrows():
        if row['patientbirthdate'] == dob and row['studyid'] == enum:
            return row[id_label]
    return False
        
def getPFileHeader(ppath):
    pFile = headers.Pfile.from_file(ppath)
    headerStructList = struct_utils.dump_struct(pFile.header)
    return pFile, headerStructList

def getMRSHeaderData(headerStructList,MRSDict):
    for struct in headerStructList:
        for entry in MRSDict:
            if entry == struct.label:
                MRSDict[entry] = str(struct.value)
                break
    if MRSDict['psd_name'] != 'PROBE-P':
        return False
    return MRSDict

def checkDuplicateData(STIL_ID):
    if os.path.isdir('/RDSMount/PRJ-4DFlow/STIL_DATA/%s/001_data_archive' %STIL_ID):
        return True
    return False

def moveToWorkingDir(WORKINGDIR,STIL_ID,pPath,STRIP=False):
    '''Strip will strip filename so that it only contains PXXXX.7'''
    subjectDir = os.path.join(WORKINGDIR,STIL_ID)
    if not os.path.isdir(subjectDir):
        os.makedirs(subjectDir)
    pFileName = os.path.basename(pPath)
    
    if STRIP:
        charList = list(pFileName)
        pFileName = ''.join(charList[-8:])
        if not pFileName[0] == 'P' or not pFileName[-2:] == '.7':
            raise ValueError('Unable to strip P-file: %s' %os.path.basename(pPath))
    outPath = os.path.join(subjectDir,STIL_ID+'_'+pFileName)
    if not os.path.isfile(outPath):
        shutil.copy2(pPath,outPath)
    else:
        print '%s already exists in %s, will not overwrite\n' %(pFileName,subjectDir)
        return False
    return outPath

def anonymizeFile(pFile,STIL_ID,outPath):
    #Items to be anonymized
    anon_list = [anonymizer.AnonEntry(key='patient_name', value=STIL_ID, option_name='name', description='patient name'),
                 anonymizer.AnonEntry(key='patient_name_2', value=STIL_ID, option_name='name_2', description='patient name 2')
                 ]
    a = anonymizer.Anonymizer(anon_list)
    a.anonymize(pFile.header)
    headers.write_header(outPath, pFile.header)
    
def syncToRDS(STIL_ID,workingPath):
    print 'Syncing to RDS'
    rootDir = '/RDSMount/PRJ-4DFlow/STIL_DATA'
    suppDir = '000_data_archive/002_converted_data/004_supplementary_data'
    subjectDir = os.path.join(rootDir,STIL_ID,suppDir)
    pFileName = os.path.basename(workingPath)
    if os.path.isdir(subjectDir):
        outPath = os.path.join(subjectDir,pFileName)
        if os.path.isfile(outPath):
            print '%s already exists on RDS, will not copy\n' %pFileName
            return False
        else:
            shutil.copyfile(workingPath,outPath)
            os.remove(workingPath)
    else:
        print '%s does not exist, will not copy to RDS\n' %subjectDir
        return False
    return outPath

def writeToCSVOutput(RDSPath,STIL_ID,MRSDict):
    TE = float(MRSDict['te']) / 1000000
    sd = MRSDict['series_description']
    if 'ROI' in sd:
        ROI = sd[sd.find('ROI') + 3]
    else:
        ROI = 'N/A'
    csvOutput = os.path.join(os.path.dirname(RDSPath),STIL_ID+'_MRS.csv')
    csvStr = ''
    if not os.path.isfile(csvOutput):
        csvStr += 'P-file,Echo time (s), ROI\n'
    csvStr += '%s,%s,%s\n' %(os.path.basename(RDSPath),TE,ROI)
    with open(csvOutput, 'a') as myfile:
        myfile.write(csvStr)
    print 'Done\n'

### MAIN ###
def runMRSFile(pPath,WORKINGDIR,STRIP):
    '''Runs for one P-file'''
    #Creates empty MRS Dict
    MRSDict = createMRSDict()
    #Generate P-file header as list of structures
    pFile, structList = getPFileHeader(pPath)
    #Grab meaningful entries in header as Dictionary
    MRSDict = getMRSHeaderData(structList, MRSDict)
    if not MRSDict:
        print 'Discarding %s as pulse sequence is not PROBE-P\n' %(pPath)
        return
    #Get associated STIL ID
    STIL_ID = matchIDs(redcapData, MRSDict['exam_number'], MRSDict['date_of_birth'], 'stil_id')
    if STIL_ID:
        print 'Found STIL ID: %s for %s' %(STIL_ID,pPath)
    else:
        print 'No STIL ID found for %s\n' %(pPath)
        return
    #Check that only 000 exists
    if checkDuplicateData(STIL_ID):
        print '001 data archive found, exiting'
        exit()
    #Move file to WORKINGDIR
    workingPFile = moveToWorkingDir(WORKINGDIR, STIL_ID, pPath, STRIP)
    if workingPFile:
        #Anonymize file
        anonymizeFile(pFile,STIL_ID,workingPFile)
        #Copy files to RDSMount
        RDSPath = syncToRDS(STIL_ID,workingPFile)
        if RDSPath:
            #Write header info to csv
            writeToCSVOutput(RDSPath,STIL_ID,MRSDict)
        
            
if __name__ == '__main__':
    
    #------------------------
    # Argument Parsing
    # -----------------------
    ap = argparse.ArgumentParser(description='MRS to RDS Import Tool')  
    
    sp = ap.add_argument_group('Script Parameters')
    sp.add_argument('-D',dest='INDIR',help='Directory containing P-files',type=str,required=True)
    sp.add_argument('-workingDir',dest='WORKINGDIR',help='Working directory, default is /Volume/MRS',type=str,default='/Volume/MRS')
              
    args = ap.parse_args()
    
    ###INPUTS###
    cred = '/RDSMount/PRJ-STILNeuro/PROJECTS/Dashboard/credentials.txt'
    report_id = '2388' #Create better report for this, or edit
    ############
    
    ###RUN######
    fileList = findPFiles(args.INDIR)
    api_url, api_key = rdt.GetCredentials(cred)
    redcapData = rdt.ExportRedcapReport(api_url, api_key, report_id, indexname=False)
    for pPath in fileList:
        runMRSFile(pPath,args.WORKINGDIR,True)
