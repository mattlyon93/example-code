#!/usr/bin/env python


import stilpy.stilRedcap.RedCapTools as rdt
import argparse
import os

def matchAndUpdateRedcap(csv_file,api_url,api_key,FLAG,VERBOSE=False,OVERWRITE=False):
    #Converts csv to dataframe 
    data_to_import = rdt.ImportCSV(csv_file)
    if not FLAG in data_to_import.dtypes.index:
        raise ValueError('%s needs to have a %s column' %(csv_file,FLAG))
    #Gets redcad_id and input_id from redcap and converts into dataframe
    fields = ['redcap_id', FLAG]
    if VERBOSE:
        print 'Grabbing redcap IDs...'
    redcap_data = rdt.exportRedcapData(api_url, api_key, redcap_fields=fields,indexname='redcap_id')
    if VERBOSE:
        print 'Done'
    #Matches IDs in input csv
    if VERBOSE:
        print 'Matching IDs...'
    matched_ids = rdt.matchIDs(data_to_import, FLAG, redcap_data, FLAG)
    if VERBOSE:
        print 'Done'
    #Prints IDs unable to match
    if len(data_to_import.index) != len(matched_ids):
        print 'Unable to match the following IDs:'
        for index, row in data_to_import.iterrows():
            if not row[FLAG] in matched_ids:
                print(data_to_import[FLAG][index])
                data_to_import.drop([index], inplace=True)
    if FLAG == 'stil_id':
        #Checks that only 000 or 001 data archive exist
        if VERBOSE:
            print 'Checking STIL IDs only contain one dataset...'
        neuro_root_dir = '/RDSMount/PRJ-Neuro/'
        cardiac_root_dir = '/RDSMount/PRJ-4DFlow/'
        for STIL_ID in matched_ids:
            #Neuro
            if STIL_ID[3] == '6':
                if os.path.isdir(os.path.join(neuro_root_dir,STIL_ID,'000_data_archive')) and os.path.isdir(os.path.join(neuro_root_dir,STIL_ID,'001_data_archive')):
                    print '%s has both 000 & 001 data archive, exiting' %STIL_ID
                    exit()
            #Cardiac
            elif STIL_ID[3] == '2':
                if os.path.isdir(os.path.join(cardiac_root_dir,STIL_ID,'000_data_archive')) and os.path.isdir(os.path.join(cardiac_root_dir,STIL_ID,'001_data_archive')):
                    print '%s has both 000 & 001 data archive, exiting' %STIL_ID
                    exit()
        if VERBOSE:
            print 'ALL OKAY'    
    #Adds redcap ID to input csv
    if VERBOSE:
        print 'Adding redcap IDs to csv...'
    data_to_import['redcap_id'] = data_to_import[FLAG].map(matched_ids)
    #Sets redcap_id as index
    data_to_import.set_index('redcap_id', inplace=True)
    #Removes Input IDs so as to not accidentally overwrite
    data = data_to_import.drop(FLAG,1)
    if VERBOSE:
        print 'Done'
    #Uploads to redcap
    if OVERWRITE:
        overwrite = 'overwrite'
    else:
        overwrite = 'normal'
    rdt.importRecords(api_url,api_key,data,overwrite=overwrite)
    
if __name__ == '__main__':
    
    ap = argparse.ArgumentParser(description='Update Redcap Tool')
    
    sp = ap.add_argument_group('Parameters')
    sp.add_argument('-STIL',dest='FLAG',help='Matches your csv with redcap based on the STIL ID',action='store_const',const='stil_id')
    sp.add_argument('-CHRONIC',dest='FLAG',help='Matches your csv with redcap based on the CHRONIC ID',action='store_const',const='s_chronic_id')
    sp.add_argument('-HCP',dest='FLAG',help='Matches your csv with redcap based on the HCP ID',action='store_const',const='s_hcp_id')
    sp.add_argument('-CSV',dest='csv_file',help='Input CSV of records to update',type=str,required=True)
    sp.add_argument('-OVERWRITE',dest='OVERWRITE',help='Will set blank fields as empty in redcap',action='store_true',default=False)
    
    op = ap.add_argument_group('Optional')
    op.add_argument('-q',dest='VERBOSE',help='Does not display progress messages (Quiet)',action='store_false',default=True)
    
    args = ap.parse_args()
    
    api_url, api_key = rdt.GetCredentials('path/to/credentials.txt')
    
    if not os.path.isfile(args.csv_file):
        raise OSError('%s is not a file' %args.csv_file)
    #RUN
    if args.FLAG:
        matchAndUpdateRedcap(args.csv_file,api_url,api_key,args.FLAG,args.VERBOSE,args.OVERWRITE)
    else:
        print 'Error: need matching flag'
    
