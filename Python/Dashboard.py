#!/usr/bin/env python
'''
Created on 23/05/2018

Script for various Dashboard tools

@author: Matt Lyon
'''
import RedCapTools as rdt
import argparse
import os
import pandas as pd
import Gsheet_tools as gst
import subprocess
import time
import datetime
import numpy as np
import sys
import fnmatch

def CreateProjectDict(projectReportFile):
    projectDict = {}
    if os.path.isfile(projectReportFile):
        with open(projectReportFile,'r') as fn:
            for line in fn:
                if not line.startswith('#'):
                    try:
                        projname, report_id = line.split('=')
                    except:
                        print '[ERROR]: Failed to parse %s' %line
                    projectDict[projname] = report_id.rstrip()
    else:
        print '[ERROR]: Failed to find Project_Reports.txt'
        exit()
    return projectDict

def GetCredentials(credentialsFile):
    if os.path.isfile(credentialsFile):
        try:
            with open(credentialsFile,'r') as fn:
                for line in fn:
                    key, entry = line.split('=')
                    if key == 'api_key':
                        api_key = entry.rstrip()
                    elif key == 'api_url':
                        api_url = entry.rstrip()
        except:
            print '[ERROR]: Reading credentials file'
            exit()
    else:
        print '[ERROR]: Credentials file not found'
        exit()
    return api_url, api_key

class ProgressBar:
    def __init__(self,title,length):
        if not args.quiet:
            self.progress_x = 0
            self.total_length = length
            sys.stdout.write(title + ": [" + "-"*40 + "]" + chr(8)*41)
            sys.stdout.flush()

        
    def addto(self,index):
        if not args.quiet:
            x = int((float(index+1)/float(self.total_length))*float(100))
            x = int(x * 40 // 100)
            sys.stdout.write("#" * (x - self.progress_x))
            sys.stdout.flush()
            self.progress_x = x
        
    def end(self):
        if not args.quiet:
            sys.stdout.write("#" * (40 - self.progress_x) + "]\n")
            sys.stdout.flush()

def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size

def get_count(start_path = '.'):
    count = sum([len(files) for dirpath, dirnames, files in os.walk(start_path)])
    return count

def UpdateProjectDetails(projectDetailsFile):
    n_expectedDict = {}
    priorityDict = {}
    if os.path.isfile(projectDetailsFile):
        with open(projectDetailsFile) as fp:
            for line in fp:
                if not line.startswith('#'):
                    try:
                        project_code, n_expected, priority = line.rstrip().split('|')
                    except ValueError:
                        print 'Unable to parse line "%s" in Project_Details.txt' %line.rstrip()
                    n_expectedDict[project_code.strip()] = str(n_expected.strip())
                    priorityDict[project_code.strip()] = str(priority.strip())
    else:
        raise ValueError('Cannot find Project_Details.txt')
    return n_expectedDict, priorityDict

def OutputResults(project,projectOutput,spreadsheetOutput,outputDir):
    if not args.quiet:
        print 'Outputting results...'
    # Creates directory if it doesn't exist
    if not os.path.isdir('%s/%s' %(outputDir,project)):
        os.makedirs('%s/%s' %(outputDir,project))
    # Removes previous text file & writes to new
    for key in projectOutput:
        for file in os.listdir('%s/%s' %(outputDir,project)):
            if fnmatch.fnmatch(file, '*%s.txt' %key):
                os.remove('%s/%s/%s' %(outputDir,project,file))
        outputFileDir = '%s/%s/%s_%s.txt' %(outputDir,project,datetime.date.today(),key)
        outputFile = open(outputFileDir,'w')
        for entry in projectOutput[key]:
            outputFile.write('%s\n' % entry)
    if not args.quiet:
        print 'Done'
    # Update gsheet
    if not args.quiet:
        print 'Updating gsheet...'
    wks = gst.InitiateGsheet(json_key_file,dashboard_gsheet,worksheet_name)
    for key in spreadsheetOutput:
        gst.UpdateCell(wks,project,key,spreadsheetOutput[key])
    gst.UpdateCell(wks,'Study name','Last updated:',str(datetime.datetime.now().replace(microsecond=0)))
    if not args.quiet:
        print 'Done'

def RunNeuroProjectTest(project,outputDir):
    '''Runs tests for a Neuro project'''

    def ConsentCheck(data):
        '''Counts number of STIL IDs with consent present'''
        hasConsent = []
        noConsent = []
        for index,row in data.iterrows():
            if row['consent_present'] == '1':
                hasConsent.append(row['redcap_id'])
            else:
                noConsent.append(row['redcap_id'])
        return hasConsent, noConsent
        
    def DICOMCheck(IDList):
        '''Counts number of files/folders in dicom directory (not subdirectory so as to speed it up)'''
        folderSizes = {}
        notProcessed = []
        Pass = []
        Fail = []
        progress = ProgressBar('DICOMs',len(IDList))
        for index, ID in enumerate(IDList):
            dicomDir = '/Volume/%s/000_data_archive/002_converted_data/000_dicom' % ID
            if os.path.isdir(dicomDir):
                if sum(os.path.getsize(f) for f in os.listdir('.')) > 0:
                    Pass.append(ID)
                else:
                    notProcessed.append(ID)
            else:
                notProcessed.append(ID)
            progress.addto(index)   
        progress.end()
        return notProcessed, Pass
            
    def niftiCheck(IDList):
        '''Checks 002_converted_data folder to see if it contains nifti files'''
        notProcessed = []
        Pass = []
        Fail = []
        progress = ProgressBar('NIFTI',len(IDList))
        #Iterates through IDs, checks if a .nii.gz file exists
        for index, ID in enumerate(IDList):
            niftiDir = '/Volume/%s/000_data_archive/002_converted_data/Niftis' % ID
            if os.path.isdir(niftiDir):
                 size = get_size(niftiDir)
                 if size == 0:
                    notProcessed.append(ID)
                 else:
                    niftiFile = False
                    for root, dirs, files in os.walk(niftiDir):
                        for name in files:
                            if fnmatch.fnmatch(name, '*.nii.gz'):
                                niftiFile = True
                    if niftiFile:
                        Pass.append(ID)
                    else:
                        notProcessed.append(ID)
            else:
                notProcessed.append(ID)
            progress.addto(index)
        progress.end()
        return notProcessed, Pass, Fail
        
    def freesurferCheck(IDList):
        '''
        Checks Brain segmentation volume & LH Cortical thickness
        '''
        brainVols = {}
        cortThickness = {}
        notProcessed = []
        Pass = []
        Fail = []
        progress = ProgressBar('Freesurfer',len(IDList))
        for index, ID in enumerate(IDList):
            freesurferDir = '/Volume/%s/000_data_archive/006_freesurfer_data/%s/stats' % (ID,ID)
            if os.path.isdir(freesurferDir):
                if os.path.isfile('%s/lh.aparc.stats' % freesurferDir):
                    try:
                        with open('%s/lh.aparc.stats' % freesurferDir) as fp:
                            for i, line in enumerate(fp):
                                if i == 20:
                                    #21st line
                                    lineSplit = line.split(' ')
                                    cortThickness[ID] = float(lineSplit[6].rstrip(','))
                                elif i > 20:
                                    break
                        with open('%s/aseg.stats' % freesurferDir) as fp:
                            for i, line in enumerate(fp):
                                if i == 13:
                                    #14th line
                                    lineSplit = line.split(' ')
                                    brainVols[ID] = float(lineSplit[7].rstrip(','))
                                elif i > 13:
                                    break
                    except:
                        Fail.append(ID)
                        print(ID)
                else:
                    notProcessed.append(ID)
            else:
                notProcessed.append(ID)
            progress.addto(index)
        
        # Run Stats, may want to change the values here
        if len(cortThickness) > 0 and len(brainVols) > 0:
            min_brainvol_thr = 890000.0
            max_brainvol_thr = 1600000.0
            min_cort_thr = 2.0
            max_cort_thr = 3.0
            
            for ID in brainVols:
                if min_brainvol_thr <= brainVols[ID] <= max_brainvol_thr and min_cort_thr <= cortThickness[ID] <= max_cort_thr:
                    Pass.append(ID)
                else:
                    Fail.append(ID)
        progress.end()
        return notProcessed, Pass, Fail
        
    def mrtrixCheck(IDList,project):
        '''Contains methods for checking MRtrix, semi-project specific'''
        
        mrtrix141Checklist = ['STUDY1','STUDY2']
        simpleChecklist = ['STUDY3','STUDY5','STUDY7']
        skipChecklist = ['STUDY10']
        
        notProcessed = []
        Pass = []
        Fail = []
        
        def mrtrixEddyUnwarpedCheck(mrtrixSubDir,fileSizeThreshold):
            '''Checks for eddy_unwarped_images in mrtrix, and checks whether it is above size threshold'''
            fileSizes = {}
            for index, ID in enumerate(IDList):
                mrtrixDir = '/Volume/%s/000_data_archive/007_MRtrix_Processed_Data/%s' % (ID,mrtrixSubDir)
                if os.path.isdir(mrtrixDir):
                    size = get_size(mrtrixDir)
                    if size == 0:
                        notProcessed.append(ID)
                    else:
                        if os.path.isfile('%s/eddy_unwarped_images%s.nii.gz' %(mrtrixDir,ID)):
                            fileSizes[ID] = os.path.getsize('%s/eddy_unwarped_images%s.nii.gz' %(mrtrixDir,ID))
                        else:
                            notProcessed.append(ID)
                else:
                    notProcessed.append(ID)
                progress.addto(index) 
            #Run stats on eddy_unwarped_images.nii.gz    
            for ID in fileSizes:
                if fileSizes[ID] > fileSizeThreshold:
                    Pass.append(ID)
                else:
                    Fail.append(ID)
            progress.end()
            return notProcessed, Pass, Fail
        
        def simpleCheck():
            '''Checks if 007_MRtrix contains any .mif files'''
            for index, ID in enumerate(IDList):
                mrtrixDir = '/Volume/%s/000_data_archive/007_MRtrix_Processed_Data' % ID
                if os.path.isdir(mrtrixDir):
                    size = get_size(mrtrixDir)
                    if size == 0:
                        notProcessed.append(ID)
                    else:
                        mifFile = False
                        for root, dirs, files in os.walk(mrtrixDir):
                            for name in files:
                                if fnmatch.fnmatch(name, '*.mif'):
                                    mifFile = True
                        if mifFile:
                            Pass.append(ID)
                        else:
                            notProcessed.append(ID)
                else:
                    notProcessed.append(ID)
                progress.addto(index)
            progress.end()
            return notProcessed, Pass, Fail
        # Run MRtrix Checks============   
        progress = ProgressBar('MRtrix',len(IDList))
        if project in mrtrix141Checklist:
            notProcessed, Pass, Fail = mrtrixEddyUnwarpedCheck('output_140B', 280000000)
            return notProcessed, Pass, Fail
        elif project in simpleChecklist:
            notProcessed, Pass, Fail = simpleCheck()
            return notProcessed, Pass, Fail
        elif project in skipChecklist:
            return notProcessed, Pass, Fail
        else:
            notProcessed, Pass, Fail = mrtrixEddyUnwarpedCheck('output_140B', 280000000)
            return notProcessed, Pass, Fail
        # ==============================        
        
    ##==================================
    ## RUN 
    ##==================================
    if not args.quiet:
        print 'Starting Checks for %s' % (project)
    projectOutput = {}
    spreadsheetOutput = {}
    ## Generate Initial ID list
    if not args.quiet:
        print 'Generating ID list...'
    data = rdt.ExportRedcapReport(api_url, api_key, projectDict[project])
    neuro_data = data[data['data_type'].str.contains('1')]
    redcap_IDs = rdt.DFToIDs(neuro_data,'redcap_id')
    ## Generate STIL ID list
    IDList = rdt.DFToIDs(neuro_data,'ID')
    ## Runs Demographic check
    if args.demographic:
        ## Updates n expected & Priority
        try:
            spreadsheetOutput['n expected'] = n_expectedDict[project]
            spreadsheetOutput['Priority'] = priorityDict[project]
        except:
            pass
        projectOutput['Participant_number'] = redcap_IDs
        spreadsheetOutput['n'] = len(redcap_IDs)
        projectOutput['IDs_number'] = IDList
        spreadsheetOutput['ID'] = len(IDList)
        ## Check consents
        projectOutput['has_consent'], projectOutput['no_consent'] = ConsentCheck(neuro_data)
        spreadsheetOutput['Missing Consents'] = len(projectOutput['no_consent'])
    if not args.quiet:
        print 'Done'
    ## Run DICOM check
    if args.DICOM:
        projectOutput['dicom_missing'], projectOutput['dicom_pass'] = DICOMCheck(IDList)
        spreadsheetOutput['Missing DICOMs'] = len(projectOutput['dicom_missing'])
        spreadsheetOutput['DICOMs'] = len(projectOutput['dicom_pass'])
    ## Run nifti check
    if args.nifti:
        projectOutput['nifti_unprocessed'], projectOutput['nifti_pass'], projectOutput['nifti_fail'] = niftiCheck(IDList)
        spreadsheetOutput['Niftis'] = len(projectOutput['nifti_pass'])
        spreadsheetOutput['Unprocessed Niftis'] = len(projectOutput['nifti_unprocessed'])
    ## Run freesurfer check
    if args.freesurfer:
        projectOutput['fs_unprocessed'], projectOutput['fs_pass'], projectOutput['fs_fail'] = freesurferCheck(IDList)
        spreadsheetOutput['Freesurfer'] = len(projectOutput['fs_pass'])
        spreadsheetOutput['Unprocessed Freesurfer'] = len(projectOutput['fs_unprocessed'])
        spreadsheetOutput['Failed Freesurfer'] = len(projectOutput['fs_fail'])
    ## Run mrtrix check
    if args.mrtrix:
        projectOutput['mrtrix_unprocessed'], projectOutput['mrtrix_pass'], projectOutput['mrtrix_fail'] = mrtrixCheck(IDList,project)
        spreadsheetOutput['MRtrix'] = len(projectOutput['mrtrix_pass'])
        spreadsheetOutput['Unprocessed MRtrix'] = len(projectOutput['mrtrix_unprocessed'])
        spreadsheetOutput['Failed MRtrix'] = len(projectOutput['mrtrix_fail'])
    ## Output to textfiles & gsheet
    if len(spreadsheetOutput) > 0 and not args.TEST:
        OutputResults(project, projectOutput, spreadsheetOutput, outputDir)
    

if __name__ == '__main__':
    
    #------------------------
    # Argument Parsing
    # -----------------------
    ## Inputs
    json_key_file = '/RDSMount/PRJ-STILNeuro/PROJECTS/Dashboard/Dashboard.json'
    outputDir = '/RDSMount/PRJ-STILNeuro/PROJECTS/Dashboard/Project_outputs'
    projectReportFile = '/RDSMount/PRJ-STILNeuro/PROJECTS/Dashboard/Project_Reports.txt'
    projectDetailsFile = '/RDSMount/PRJ-STILNeuro/PROJECTS/Dashboard/Project_Details.txt'
    credentials = '/RDSMount/PRJ-STILNeuro/PROJECTS/Dashboard/credentials.txt'
    dashboard_gsheet = 'DASHBOARD_STIL'
    worksheet_name = 'DASHBOARD'
    
    ap = argparse.ArgumentParser(description='Dashboard Tools')
    
    sp = ap.add_argument_group('Required Parameters')
    sp.add_argument('-P',dest='project',help='Updates selected project in dashboard',type=str)
    sp.add_argument('-A',dest='ALL',help='Updates dashboard for all known projects',action='store_true')
    
    op = ap.add_argument_group('Optional Parameters')
    op.add_argument('-d',dest='demographic',help='Updates ONLY demographic data in dashboard',action='store_true',default=False)
    op.add_argument('-D',dest='DICOM',help='Updates ONLY DICOMs in dashboard',action='store_true',default=False)
    op.add_argument('-N',dest='nifti',help='Updates ONLY Niftis in dashboard',action='store_true',default=False)
    op.add_argument('-F',dest='freesurfer',help='Updates ONLY Freesurfer in dashboard',action='store_true',default=False)
    op.add_argument('-M',dest='mrtrix',help='Updates ONLY MRtrix in dashboard',action='store_true',default=False)
    op.add_argument('-q',dest='quiet',help='Does not display progress',action='store_true',default=False)
    op.add_argument('-T',dest='TEST',help='Testing',action='store_true',default=False)
    
    args = ap.parse_args()
    
    projectDict = CreateProjectDict(projectReportFile)
    api_url, api_key = GetCredentials(credentials)
    
    if not args.demographic and not args.DICOM and not args.nifti and not args.freesurfer and not args.mrtrix:
        args.demographic = True
        args.DICOM = True
        args.nifti = True
        args.freesurfer = True
        args.mrtrix = True
    
    if args.demographic:
        n_expectedDict, priorityDict = UpdateProjectDetails(projectDetailsFile)
     
    if args.ALL:
        for project in projectDict:
            RunNeuroProjectTest(project, outputDir)
    elif args.project:
        '''
        Runs project specific test
        '''
        if args.project in projectDict:
            RunNeuroProjectTest(args.project, outputDir)
        else:
            raise NameError('Incorrect Project Code')
