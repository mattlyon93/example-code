//
//  metronome_improved.swift
//  Drummer Drills
//
//  Created by Matt Lyon on 5/12/17.
//  Copyright © 2017 Matt Lyon. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class Metronome {
 
    var timeSig:String = "68"
    var bpm:Double = 60
    var subDiv:Int = 1
    var wasPlaying = false
    var audioEngineDict = [Int:AVAudioEngine]()
    var audioNodeDict = [Int:AVAudioPlayerNode]()
    var audioFileDict = [Int:AVAudioFile]()
    var bufferDict = [Int:AVAudioPCMBuffer]()
    var delayTimeDict = [Int:Double]()
    var delayFrameDict = [Int:AVAudioFramePosition]()
    var startTimeDict = [Int:AVAudioTime]()
    let outputFormat:AVAudioFormat
    let sampleRateBPM:Double
    
    
    init (fileURL1: URL, fileURL2: URL) {

        audioFileDict[0] = try! AVAudioFile(forReading: fileURL1)
        audioFileDict[1] = try! AVAudioFile(forReading: fileURL2)
    
        //Create Engines
        for n in 0...3 {
            self.audioEngineDict[n] = AVAudioEngine()
        }
        //Create Nodes
        for n in 0...31 {
            self.audioNodeDict[n] = AVAudioPlayerNode()
        }
        
        //Attach & Connect Nodes
        audioEngineDict[0]!.attach(self.audioNodeDict[0]!)
        audioEngineDict[0]!.connect(self.audioNodeDict[0]!, to: audioEngineDict[0]!.mainMixerNode, format: audioFileDict[0]!.processingFormat)
        for n in 1...31 {
            var i:Int
            switch n {
            case 1...7:
                i = 0
            case 8...15:
                i = 1
            case 16...23:
                i = 2
            case 24...31:
                i = 3
            default:
                i = 0
            }
            audioEngineDict[i]!.attach(self.audioNodeDict[n]!)
            audioEngineDict[i]!.connect(self.audioNodeDict[n]!, to: audioEngineDict[i]!.mainMixerNode, format: audioFileDict[1]!.processingFormat)
        }
        
        //Start your engines
        for (i, _) in audioEngineDict {
            try! self.audioEngineDict[i]!.start()
        }
        
        outputFormat = audioNodeDict[0]!.outputFormat(forBus:0)
        sampleRateBPM = outputFormat.sampleRate * 60 / bpm
        
    }
    
    func start(_ t:Int,_ b:Int) {
        
        var d:Int
        switch subDiv {
        case 2:
            d = 2 * Int(t)
        case 3:
            d = 4 * Int(t)
        default:
            d = 1 * Int(t)
        }
        
        var tx:Double
        switch b {
        case 8:
            tx = Double(t) / 2
        default:
            tx = Double(t)
        }
        //Set Buffer
        for n in 0...1 {
            self.bufferDict[n] = generateBuffer(audioFileDict[n]!,bpm,BeatsPerBar:tx)
        }
        //Set Delay time
        for n in 0...d-2 {
            self.delayTimeDict[n] = ( Double(n+1) / Double(d) ) * ( 60 / bpm ) * tx
        }
        
        //Set Delay frame
        for n in 0...d-2 {
            self.delayFrameDict[n] = AVAudioFramePosition.init(delayTimeDict[n]! * outputFormat.sampleRate)
        }
        
        //Set initial frame
        let startFrame = audioNodeDict[0]!.lastRenderTime?.sampleTime
        
        //Set Start times
        self.startTimeDict[0] = AVAudioTime.init(sampleTime: startFrame!, atRate: sampleRateBPM)
        for n in 1...d-1 {
            self.startTimeDict[n] = AVAudioTime.init(sampleTime: startFrame! + delayFrameDict[n-1]!, atRate: sampleRateBPM)
        }
            
        //Set Buffers
        audioNodeDict[0]!.scheduleBuffer(bufferDict[0]!, at: nil, options: .loops, completionHandler: nil)
        for n in 1...d-1 {
            audioNodeDict[n]!.scheduleBuffer(bufferDict[1]!, at: nil, options: .loops, completionHandler: nil)
        }
        
        //Play
        for n in 0...d-1 {
            audioNodeDict[n]!.play(at: self.startTimeDict[n]!)
        }
    }
    
    func play() {
        let timeSigArray = timeSig.flatMap{Int(String($0))}
        self.start(timeSigArray[0],timeSigArray[1])
    }
    
    func stop() {
        for (_,node) in audioNodeDict {
            node.stop()
        }
    }
    
    func isPlaying() -> Bool {
        return audioNodeDict[0]!.isPlaying
    }
}








