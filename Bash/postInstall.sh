#!/bin/bash

usage()
{
cat << EOF
usage: $0 options

Script to set up machine

MAIN:
	-U	ADD USER
	-I	INSTALL Packages
	-C	Checks Package installation status
	-S	Install STIL git repository
	-R	Mounts RDS with credentials file
	-E	INSTALL ALL Extra Software
	-T	TEST Extra software installation

SEPERATE:
	COVERED BY -I:
	-install_apacks	Installs apt packages
	-install_ppacks	Installs pip packages
	
	COVERED BY -E:
	-install_mrtrix	Installs MRtrix3
	-install_fsl	Installs FSL
	-install_fs	Installs FreeSurfer
	-install_ants	Installs ANTs
	-install_pview	Installs ParaView

OTHER:
	-h	Shows this message   
    
EOF
}

checkRoot()
{
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
}


# ==============================================================================
OPTIND=1
OWNER=""
ADD_USER=False
ins_ap=False
ins_pp=False
CHECK=False
STIL=False
RDS=False
EXTRA=False
ins_mrtrix=False
ins_fsl=False
ins_fs=False
ins_ants=False
ins_pview=False
TEST=False

while [[ $# > 0 ]];
do
	key="$1"
	shift
	case $key in
		-U|--user)
    	ADD_USER=True
	   	;;
	    -I|--install)
	    ins_ap=True
		ins_pp=True
	    ;;
		-install_apacks)
		ins_ap=True
		;;
		-install_ppacks)
		ins_pp=True
		;;
		-C|--check)
		CHECK=True
		;;
		-S|--STIL)
		STIL=True
		;;
	    -E|--extra)
		EXTRA=True
		T=True
	    ;;
		-R|--RDSMount)
		RDS=True
		;;
		-install_mrtrix)
		ins_mrtrix=True
		;;
		-install_fsl)
		ins_fsl=True
		;;
		-install_fs)
		ins_fs=True
		;;
		-install_ants)
		ins_ants=True
		;;
		-install_pview)
		ins_pview=True
		;;
		-T|--Test)
		TEST=True
		;;
	    -h|--help)
	    usage
	    exit 1
	    ;;
	    *)
	    echo ""
    echo " *** ERRROR - UNKNOWN OPTION ****"
    echo ""
    usage
    exit 1
    ;;
esac
done
# ==============================================================================
# ==============================================================================

checkRoot
# ==============================================================================

if [ "$ins_ap" = "True" ];
then

	# === INSTALL APT PACKAGES ===
    echo " INSTALL"
    echo "   RUN UPDATE UPGRADE"
    apt-get -y update
    apt-get -y upgrade
    apt-get -y update
    apt-get -y upgrade
    echo " ---- "

	IFS=$'\n'
    echo "  INSTALL APT PACKAGES"
	for package in $(<./apt_packages.txt)
	do
		if [[ ! ${package:0:1} == '#' ]];
		then
    		apt-get install -y $package
		fi
	done
	echo " ---- "
fi

if [ "$ins_pp" = "True" ];
then
	# === INSTALL PIP PACKAGES ===
	echo "  INSTALL PIP PACKAGES"
	IFS=$'\n'
	pip install --upgrade pip
	if ! pip --version > /dev/null 2>&1;
	then
		hash -d pip
	fi
	pip install --upgrade cryptography
	for package in $(<./pip_packages.txt)
	do
		if [[ ! ${package:0:1} == '#' ]];
		then
			pip install $package
		fi
	done
	python -m easy_install --upgrade pyOpenSSL
    echo "------------------------ "
fi

# ==============================================================================

if [ "$CHECK" = "True" ]; then

	# === CHECK APT & PIP PACKAGE INSTALLATION ===
	echo "	APT-GET PACKAGES CHECK"
	echo "*If not installed, please manually install using 'sudo apt-get install PKG'"
	IFS=$'\n'
	for package in $(<./apt_packages.txt)
	do
		if [[ ! ${package:0:1} == '#' ]];
		then
			if ! dpkg -s $package > /dev/null 2>&1;
			then
				echo "$package NOT INSTALLED"
			fi
		fi
	done
	echo "	DONE"
	echo
	echo "	PIP PACKAGES CHECK"
	echo "*If not installed, please manually install using 'sudo pip install PKG'"
	for package in $(<./pip_packages.txt);
	do
		if [[ ! ${package:0:1} == '#' ]]; then
			if ! pip show $package > /dev/null 2>&1;
			then
				echo "$package NOT INSTALLED"
			fi
		fi
	done
	echo "	DONE"
fi

# ==============================================================================

if [ "$ADD_USER" = "True" ];
then

	# === ADD STIL_USER ===
    newUser="stil_user"
    echo " ADDING USER: $newUser" 
    adduser --quiet --disabled-password --shell /bin/bash --home /home/$newUser --gecos "User" $newUser
    echo "$newUser:$newUser" | chpasswd
	chgrp $newUser /Volume
	chgrp $newUser /Backup
	chmod 2775 /Volume -R
	chmod 2775 /Backup -R
    echo "------------------------ "
fi

# ==============================================================================

if [ "$STIL" = "True" ];
then

	# === STIL GIT REPOSITORY ===
	echo "	STILPY REPOSITORY"
	mkdir /opt/DEV/
	sudo chown stiladmin:stiladmin /opt/DEV/
	echo "Please add this computer to machines list and run SyncCPCMachines.sh to sync repository"
	read
	echo "source /opt/DEV/stilpy/stilpy/MachineSetup/envSetup.sh" >> /home/stiladmin/.bashrc
	echo "source /opt/DEV/stilpy/stilpy/MachineSetup/envSetup.sh" >> /home/stil_user/.bashrc

fi

# ==============================================================================

if [ "$RDS" = "True" ];
then

	# === RDS Mount ===
	fstab=/etc/fstab
	echo "# === RDS ===" >> $fstab
	echo "//example.address/SMS /RDSMount cifs  credentials=/root/.rdscred,uid=1000,gid=1001,rw,file_mode=0774,dir_mode=0774,iocharset=utf8   0       0" >> $fstab
	mkdir /RDSMount
	mount -a
	if [ ! -d /RDSMount/PRJ-STILNeuro ];
	then
		echo "***ERROR Mounting RDS"
		exit 1
	fi
	automount=/etc/network/if-up.d/mountRDS
	echo "#!/bin/bash" > $automount
	echo "mount -a" >> $automount
	chmod 755 $automount
	if [ -d /RDSMount/PRJ-STILNeuro ];
	then
		echo "Mounting succesful"
	else
		echo "ERROR: RDS Mounting unsuccesful"
	fi
	#This is the only semi appropriate place I thought i could put this..
	cp /RDSMount/PRJ-4DFlow/SOFTWARE/dcm2niix/dcm2niix /usr/bin/dcm2niix
	chmod 755 /usr/bin/dcm2niix
fi

# =============================================================================

if [ "$ins_mrtrix" = "True" ] || [ "$EXTRA" = "True" ];
then
    # ==== MRTRIX ====
	echo "   MRTRIX INSTALL"
    mrtrixRoot=/opt/mrtrix
    mkdir -p $mrtrixRoot
    cd $mrtrixRoot
    git clone https://github.com/MRtrix3/mrtrix3.git
    cd mrtrix3
    ./configure
    ./build
    echo " ---- "
fi

if [ "$ins_fsl" = "True" ] || [ "$EXTRA" = "True" ];
then
	# ==== FSL ====
	echo "	FSL INSTALL"
	echo "ENSURE INSTALLATION DIRECTORY IS '/usr/local/fsl'"
	sleep 3
	/RDSMount/PRJ-4DFlow/SOFTWARE/FSL/fslinstaller.py
fi

if [ "$ins_fs" = "True" ] || [ "$EXTRA" = "True" ];
then
    # ==== FREESURFER ====
	echo "   FREESURFER INSTALL"
	freesurferTemp=/tmp/freesurfer
	freesurferSoftwareDir="/RDSMount/PRJ-4DFlow/SOFTWARE/FreeSurfer"
	freesurferFile="$freesurferSoftwareDir/freesurfer-Linux-centos6_x86_64-stable-pub-v6.0.0.tar.gz"
	installDir="/usr/local"
	mkdir -p $freesurferTemp
	cd $freesurferTemp
    cp $freesurferFile freesurfer_linux.tar.gz
    tar -C $installDir -xzvf freesurfer_linux.tar.gz
	cp $freesurferSoftwareDir/license.txt $installDir/freesurfer/license.txt
	#Install Matlab Runtime Compiler if not installed
	if [ ! -d $installDir/freesurfer/MCRv80/ ];
	then
		cp $freesurferSoftwareDir/MCR_R2012b_glnxa64_installer.zip MCR_R2012b_glnxa64_installer.zip
		unzip MCR_R2012b_glnxa64_installer.zip -d MCRv80
		./MCRv80/install -mode silent -agreeToLicense yes -destinationFolder $installDir/freesurfer/v80
		mv $installDir/freesurfer/v80 $installDir/freesurfer/MCRv80
	fi
	rm -rf $freesurferTemp
	mkdir -p /Volume/MRI_DATA/FREESURFER_WORKING
    echo " ---- "
fi

if [ "$ins_ants" = "True" ] || [ "$EXTRA" = "True" ];
then
    # ===== ANTs ======
	echo "   ANTs INSTALL"
	mkdir -p ~/code
	antsBuildDir=/opt/ants
	antsRoot="~/code/ANTs"
	cd ~/code
	git clone https://github.com/stnava/ANTs.git
	mkdir -p $antsBuildDir
	cd $antsBuildDir
	if ! cmake $antsRoot;
	then
		echo "	UPDATING CMAKE"
		apt remove -y --purge --auto-remove cmake
		mkdir -p ~/cmake && cd ~/cmake
		version="3.13.4"
		wget "https://github.com/Kitware/CMake/releases/download/v${version}/cmake-${version}.tar.gz"
		tar -xzvf cmake-${version}.tar.gz
		cd cmake-${version}
		./bootstrap
		make -j4
		make install
		rm -rf ~/cmake
		cd $antsBuildDir && cmake $antsRoot
	fi
	make -j 4
	rm -rf ~/code/
    echo " ---- "
fi

if [ "$ins_pview" = "True" ] || [ "$EXTRA" = "True" ];
then
    # ==== PARAVIEW ====
	echo "   ParaView INSTALL"
	pvSoftwareDir="/RDSMount/PRJ-4DFlow/SOFTWARE/ParaView"
    pvDir="/opt/ParaView"
	pVersion="5.2.0"
    pvName="ParaView-v${pVersion}"
    pvBuildDir="${pvDir}/ParaView-${pVersion}"
	pvGZFileName=${pvName}.tar.gz
    mkdir -p $pvBuildDir
    cd $pvDir
    cp ${pvSoftwareDir}/$pvGZFileName $pvGZFileName
    tar -zxf $pvGZFileName
    cd $pvBuildDir
    cmake -DCMAKE_BUILD_TYPE=Release -DPARAVIEW_ENABLE_PYTHON=ON -DPARAVIEW_USE_MPI=ON ../${pvName}/
    make -j 4
    cd ../
	rm -rf $pvGZFileName ${pvName}/
#    echo "------------------------ "
fi

if [ "$TEST" = "True" ];
then
	echo "	CHECKING EXTRA SOFTWARE INSTALLATION"
	declare -A software=( ["MRtrix"]="/opt/mrtrix/mrtrix3/bin/mrinfo" ["FSL"]="/usr/local/fsl/bin/fslsplit" ["FreeSurfer"]="/usr/local/freesurfer/bin/recon-all" ["ANTs"]="/opt/ants/bin/antsRegistration" ["ParaView"]="/opt/ParaView/ParaView-5.2.0/bin/paraview")
	for package in "${!software[@]}"
	do
		if [ -f "${software[$package]}" ];
		then
			echo "$package INSTALLED"
		else
			echo "ERROR: $package NOT INSTALLED CORRECTLY"
		fi
	done
fi

# ==============================================================================
