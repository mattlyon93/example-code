ver="1.2.0"
##==================================================================
## Update Packages on CPC Machines
##==================================================================
usage()
{
  cat << EOF
USAGE: 
  	UpdatePackages.sh

	Version: $ver

	Script to update packages on CPC machines

OPTIONS:

	-mrtrix	Updates mrtrix3 in /opt/mrtrix/

	-pip	Updates pip

	-pip_p	[package name] Updates a pip package

	-i	Individual update "stilXXX" (omitting this option updates all machines)

	-s	Updates your machine only

	-p	Prints list of machines only (if -i is used will only print those selected)

	-v	Displays version number

	-h	Displays this message

example:
	UpdatePackages.sh -mrtrix -i stil100
EOF
}
##------------------------------------------------------------------
if [[ $# -eq 0 ]];
then
	usage
	exit 1
fi
declare -a inputMachines=()
while [[ $# > 0 ]];
do
	case "$1" in
		-i|--Individual)
			shift
			if [[ $# > 0 ]];
			then
				inputMachines+=($1)
			else
				echo "no machine entered"
				exit 1
			fi
			shift
        	;;
		-s|--Self)
			shift
			self=true
			;;
		-p|--Print)
			p="Y"
			shift
			;;
		-pip)
			pip="Y"
			shift
			;;
		-pip_p)
			shift
			pack=$1
			shift
			;;
		-mrtrix)
			mrtrix="Y"
			shift
			;;
        -h|--help)
            usage
            exit 1
            ;;
		-v|--version)
            echo "version is: $ver"
            exit
            ;;
        *)
            usage
            exit 1
            ;;
	esac
done
##==================================================================
containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && MACHINES+=(${!match}); done
}
##------------------------------------------------------------------
if [ -d "$STILPY/stilpy/MachineSetup" ];
then
	setupDir="$STILPY/stilpy/MachineSetup"
else
	echo "STILPY path not set or incorrect, exiting"
	exit 1
fi
##------------------------------------------------------------------
IFS=$'\n'
declare -a MACHINES=()
for machine in $(tail -n +2 $setupDir/CurrentMachines)
do
	if [[ ! ${machine:0:1} == '#' ]];
	then
		fn=$(echo $machine |cut -d '=' -f 1)
		fv=$(echo $machine | cut -d '=' -f 2)
		printf -v $fn $fv
		if [ -z $inputMachines ];
		then
			MACHINES+=(${!fn})
		else
			containsElement $fn ${inputMachines[@]}
		fi
	fi
done
##=PRINT============================================================
if [ ! -z $p ] && [ ! $self ];
then
	echo "	MACHINES:"
	for iMACH in "${MACHINES[@]}"
	do
		echo "$iMACH"
	done
	exit
fi
##-MRTRIX-----------------------------------------------------------
if [ ! -z $mrtrix ];
then
	if [ $self ];
	then
		cd /opt/mrtrix/mrtrix3; sudo git pull; sudo ./configure; sudo ./build
	else
		echo "================= mrtrix3 ================= "
		for iMACH in "${MACHINES[@]}"
		do
    		echo "================= ""$iMACH"" ================= "
			ssh -t $iMACH 'cd /opt/mrtrix/mrtrix3; sudo git pull; sudo ./configure; sudo ./build'
		done
	fi
fi
##-PIP--------------------------------------------------------------
if [ ! -z $pip ];
then
	if [ $self ];
	then
		sudo pip install --upgrade pip
	else
		echo "================= pip ================= "
		for iMACH in "${MACHINES[@]}"
		do
    		echo "================= ""$iMACH"" ================= "
			ssh -t $iMACH 'sudo pip install --upgrade pip'
		done
	fi
fi
##-PIP PACKAGE------------------------------------------------------
if [ ! -z $pack ];
then
	if [ $self ];
	then
		sudo pip install $pack --upgrade
	else
		echo "================= pip: $pack ================= "
		for iMACH in "${MACHINES[@]}"
		do
    		echo "================= ""$iMACH"" ================= "
			ssh -t $iMACH sudo pip install $pack --upgrade
		done
	fi
fi
##==================================================================
